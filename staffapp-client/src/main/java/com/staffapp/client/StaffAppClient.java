package com.staffapp.client;

import com.staffapp.model.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.GET;

public class StaffAppClient {

    private static final Logger log = LoggerFactory.getLogger(StaffAppClient.class);

    private final RestTemplate restTemplate;

    public StaffAppClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Department getDepartmentById(Long departmentId) {
        log.debug("StaffAppClient: get Department: {}", departmentId);

        HttpHeaders requestHeaders = new HttpHeaders();
        HttpEntity<Department> entity = new HttpEntity<>(null, requestHeaders);
        return restTemplate.exchange("/department/find?id={departmentId}",
                GET, entity, Department.class, departmentId).getBody();
    }

}
