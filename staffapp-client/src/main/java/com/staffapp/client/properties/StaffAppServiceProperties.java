package com.staffapp.client.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "staffapp.endpoint")
public class StaffAppServiceProperties {
    private String staffAppServiceUrl;

    public String getStaffAppServiceUrl() {
        return staffAppServiceUrl;
    }

    public void setStaffAppServiceUrl(String staffAppServiceUrl) {
        this.staffAppServiceUrl = staffAppServiceUrl;
    }
}
