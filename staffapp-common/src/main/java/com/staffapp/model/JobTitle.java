package com.staffapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "job_titles")
public class JobTitle extends BaseEntity {

    @Column(name = "description")
    private String description;
    @Column(name = "min_salary")
    private Long minSalary;
    @Column(name = "max_salary")
    private Long maxSalary;

    public String getDescription() {
        return description;
    }

    public void setDescription(String jobTitle) {
        this.description = jobTitle;
    }

    public Long getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Long minSalary) {
        this.minSalary = minSalary;
    }

    public Long getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Long maxSalary) {
        this.maxSalary = maxSalary;
    }
}
