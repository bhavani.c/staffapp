package com.staffapp.bootstrap;

import com.staffapp.model.Department;
import com.staffapp.model.Employee;
import com.staffapp.model.JobTitle;
import com.staffapp.service.DepartmentService;
import com.staffapp.service.EmployeeService;
import com.staffapp.service.JobTitleService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("dev-nofile")
@Component
public class DataLoader implements CommandLineRunner {

    private final DepartmentService departmentService;
    private final EmployeeService employeeService;
    private final JobTitleService jobTitleService;

    public DataLoader(DepartmentService departmentService, EmployeeService employeeService, JobTitleService jobTitleService) {
        this.departmentService = departmentService;
        this.employeeService = employeeService;
        this.jobTitleService = jobTitleService;
    }

    @Override
    public void run(String... args) throws Exception {
        localData();
    }

    private void localData() {
        //Create Job Titles
        JobTitle engineer = new JobTitle();
        engineer.setDescription("Engineer");
        engineer.setMinSalary(123L);
        engineer.setMaxSalary(999L);
        jobTitleService.save(engineer);

        JobTitle manager = new JobTitle();
        manager.setDescription("Manager");
        manager.setMinSalary(123L);
        manager.setMaxSalary(999L);
        jobTitleService.save(manager);

        JobTitle salesRep = new JobTitle();
        salesRep.setDescription("Sales Representative");
        salesRep.setMinSalary(123L);
        salesRep.setMaxSalary(999L);
        jobTitleService.save(salesRep);

        JobTitle adminAss = new JobTitle();
        adminAss.setDescription("Administrative Assistant");
        adminAss.setMinSalary(123L);
        adminAss.setMaxSalary(999L);
        jobTitleService.save(adminAss);

        System.out.println("Loaded Job Titles");

        //Create departments
        Department hr = new Department();
        hr.setDescription("Human Resources");
        Department rAndD = new Department();
        rAndD.setDescription("Research And Development");
        Department sales = new Department();
        sales.setDescription("Sales");

        departmentService.save(hr);
        departmentService.save(rAndD);
        departmentService.save(sales);

        System.out.println("Loaded Departments");

        //Create employees
        Employee mark = new Employee();
        mark.setFirstName("Mark");
        mark.setMiddleName("Anthony");
        mark.setLastName("Muniz");
        mark.setJobTitle(manager);
        mark.setDepartment(hr);
        mark.setSalary(500L);
        employeeService.save(mark);

        Employee jane = new Employee();
        jane.setFirstName("Jane");
        jane.setLastName("Thomas");
        jane.setJobTitle(engineer);
        jane.setDepartment(rAndD);
        jane.setSalary(400L);
        employeeService.save(jane);

        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Thomas");
        john.setJobTitle(engineer);
        john.setDepartment(rAndD);
        john.setSalary(400L);
        employeeService.save(john);

        Employee mary = new Employee();
        mary.setFirstName("Mary");
        mary.setLastName("Sue");
        mary.setJobTitle(salesRep);
        mary.setDepartment(sales);
        mary.setSalary(200L);
        employeeService.save(mary);

        System.out.println("Loaded Employees");


    }
}
