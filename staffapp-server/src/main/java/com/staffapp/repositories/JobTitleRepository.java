package com.staffapp.repositories;

import com.staffapp.model.JobTitle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobTitleRepository extends CrudRepository<JobTitle, Long> {
    JobTitle findByIdOrDescription(Long id, String description);
}
