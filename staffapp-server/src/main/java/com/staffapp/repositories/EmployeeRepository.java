package com.staffapp.repositories;

import com.staffapp.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    Set<Employee> findByFirstNameOrLastName(String firstName, String lastName);
    Set<Employee> findByFirstNameOrLastNameAndMiddleName(String firstName, String lastName, String middleName);
}
