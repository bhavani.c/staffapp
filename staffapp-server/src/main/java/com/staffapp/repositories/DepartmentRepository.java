package com.staffapp.repositories;

import com.staffapp.model.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {
    Department findByIdOrDescription(Long id, String description);
}
