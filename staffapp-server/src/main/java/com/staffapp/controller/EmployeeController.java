package com.staffapp.controller;

import com.staffapp.model.Employee;
import com.staffapp.service.EmployeeService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;

@RequestMapping("/employee")
@RestController
public class EmployeeController {
    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/list")
    public Set<Employee> getAllEmployees() {
        return employeeService.findAll();
    }

    @GetMapping("/find")
    public Set<Employee> getByNames(
            @RequestParam(name = "firstName", required = false) String firstName,
            @RequestParam(name = "lastName", required = false) String lastName,
            @RequestParam(name = "middleName", required = false) String middleName) {

        return employeeService.findByNames(firstName, lastName, middleName);
    }

    @GetMapping("/get")
    public Employee getById(@RequestParam Long id) throws IOException {
        return employeeService.findById(id);
    }

    @PostMapping("/new")
    public Employee addNewEmployee(@RequestBody Employee newEmployee) throws IOException {
        if(newEmployee.getProfilePic() == null) {
            File img = new ClassPathResource("static/images/default-profile-pic.png").getFile();
            byte[] imagebytes = Files.readAllBytes(img.toPath());
            Byte[] imageBytes = new Byte[imagebytes.length];
            int i = 0;
            for (byte b : imagebytes) {
                imageBytes[i++] = b;
            }
            newEmployee.setProfilePic(imageBytes);
        }
        return employeeService.create(newEmployee);
    }

    @PutMapping("/update")
    public Employee updateEmployee(@RequestBody Employee employee) {
        if (employeeService.findById(employee.getId()) != null) {
            return employeeService.save(employee);
        }
        return null;
    }

    @DeleteMapping("/delete")
    public void deleteById(@RequestParam Long id) {
        if (employeeService.findById(id) != null) {
            employeeService.deleteById(id);
        }
    }

    @PutMapping("/changePic")
    public Employee changeProfilePic(@RequestParam Long id, @RequestBody MultipartFile profilePic) {
        Employee employee = employeeService.findById(id);
        employee.setProfilePic(getProfilePicFromMultipartFile(profilePic));
        return employeeService.save(employee);
    }

    private Byte[] getProfilePicFromMultipartFile(MultipartFile file) {
        try {
            Byte[] byteObjects = new Byte[file.getBytes().length];
            int i = 0;
            for(byte b : file.getBytes()) {
                byteObjects[i++] = b;
            }
            return byteObjects;
        } catch (IOException e) {
//            log.error("Error occurred", e);
            e.printStackTrace();
            return null;
        }
    }

    @DeleteMapping("/deleteAll")
    public void deleteAllEmployees() {
        employeeService.deleteAll();
    }

}

