package com.staffapp.controller;

import com.staffapp.model.JobTitle;
import com.staffapp.service.JobTitleService;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RequestMapping(path = "jobtitle")
@RestController
public class JobTitleController {

    private final JobTitleService jobTitleService;

    public JobTitleController(JobTitleService jobTitleService) {
        this.jobTitleService = jobTitleService;
    }

    @GetMapping("/list")
    public Set<JobTitle> getAllJobTitles() {
        return jobTitleService.findAll();
    }

    @GetMapping("/find")
    public JobTitle findByIdOrDescription(@RequestParam(name = "id", required = false) Long id,
                                            @RequestParam(name = "desc", required = false) String description) {
        return jobTitleService.findByIdOrDescription(id, description);
    }

    @PostMapping("/save-update")
    public JobTitle saveJobTitle(@RequestBody JobTitle newJobTitle) {
        return jobTitleService.save(newJobTitle);
    }

    @DeleteMapping("/delete")
    public void deleteById(@RequestParam Long id) {
        if (jobTitleService.findById(id) != null) {
            jobTitleService.deleteById(id);
        }
    }

    @DeleteMapping("/deleteAll")
    public void deleteAllJobTitles() {
        jobTitleService.deleteAll();
    }
}
