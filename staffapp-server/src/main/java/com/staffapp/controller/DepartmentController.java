package com.staffapp.controller;

import com.staffapp.model.Department;
import com.staffapp.service.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/department")
public class DepartmentController {

    private static final Logger log = LoggerFactory.getLogger(DepartmentController.class);

    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/list")
    public Set<Department> getAllDepartments() {
        return departmentService.findAll();
    }

    @GetMapping(path = "/find")
    public Department findByIdOrDescription(@RequestParam(name = "id", required = false) Long id,
                                            @RequestParam(name = "desc", required = false) String description) {
        return departmentService.findByIdOrDescription(id, description);
    }

    @PostMapping("/new")
    public Department addNewDepartment(@RequestBody Department newDepartment) {
        return departmentService.create(newDepartment);
    }

    @PutMapping("/update")
    public Department updateDepartment(@RequestBody Department department) {
        if (departmentService.findById(department.getId()) != null) {
            return departmentService.save(department);
        }
        return null;
    }

    @DeleteMapping("/delete")
    public void deleteById(@RequestParam("id") Long id) {
        if (departmentService.findById(id) != null) {
            departmentService.deleteById(id);
        }
    }

    @DeleteMapping("/deleteAll")
    public void deleteAllDepartments() {
        departmentService.deleteAll();
    }

}
