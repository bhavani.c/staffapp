package com.staffapp.service;


import com.staffapp.model.Department;

public interface DepartmentService extends CrudService<Department, Long> {
    Department create(Department newDepartment);
    Department findByIdOrDescription(Long id, String description);
}
