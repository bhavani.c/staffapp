package com.staffapp.service.impl;

import com.staffapp.model.Employee;
import com.staffapp.repositories.EmployeeRepository;
import com.staffapp.repositories.JobTitleRepository;
import com.staffapp.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final JobTitleRepository jobTitleRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, JobTitleRepository jobTitleRepository) {
        this.employeeRepository = employeeRepository;
        this.jobTitleRepository = jobTitleRepository;
    }

    @Override
    public Set<Employee> findAll() {
        Set<Employee> employees = new HashSet<>();
        employeeRepository.findAll().forEach(employees::add);
        return employees;
    }

    @Override
    public Employee findById(Long aLong) {
        return employeeRepository.findById(aLong).orElse(null);
    }

    @Override
    public Employee save(Employee employee) {
        if (employee != null) {
            if (employee.getJobTitle() != null) {
                if (jobTitleRepository.existsById(employee.getJobTitle().getId())) {
                    return employeeRepository.save(employee);
                } else {
                    throw new RuntimeException("Employee's Job Title must be an existent Job Title");
                }
            } else {
                throw new RuntimeException("Employee's Job Title is required");
            }

        }
        return null;

    }

    @Override
    public void delete(Employee object) {
        employeeRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        employeeRepository.deleteById(aLong);
    }

    @Override
    public Employee create(Employee newEmployee) {
        if (newEmployee.getId() == null || !employeeRepository.existsById(newEmployee.getId())) {
            return employeeRepository.save(newEmployee);
        }
        return null;
    }

    @Override
    public Set<Employee> findByNames(String firstName, String lastName, String middleName) {
        if(middleName == null) {
            return employeeRepository.findByFirstNameOrLastName(firstName, lastName);
        }
        return employeeRepository.findByFirstNameOrLastNameAndMiddleName(firstName, lastName, middleName);
    }

    @Override
    public void deleteAll() {
        employeeRepository.deleteAll();
    }
}
