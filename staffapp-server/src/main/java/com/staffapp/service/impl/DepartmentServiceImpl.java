package com.staffapp.service.impl;

import com.staffapp.model.Department;
import com.staffapp.repositories.DepartmentRepository;
import com.staffapp.repositories.EmployeeRepository;
import com.staffapp.repositories.JobTitleRepository;
import com.staffapp.service.DepartmentService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;
    private final JobTitleRepository jobTitleRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository,
                                 JobTitleRepository jobTitleRepository) {
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
        this.jobTitleRepository = jobTitleRepository;
    }

    @Override
    public Set<Department> findAll() {
        Set<Department> departments = new HashSet<>();
        departmentRepository.findAll().forEach(departments::add);
        return departments;
    }

    @Override
    public Department findById(Long aLong) {
        return departmentRepository.findById(aLong).orElse(null);
    }

    @Override
    public Department save(Department department) {
        if (department != null) {
            if (department.getEmployees() != null) {
                department.getEmployees().forEach(employee -> {
                    if (employee.getJobTitle() != null) {
                        if (employee.getId() != null) {
                            employee.setJobTitle(jobTitleRepository.save(employee.getJobTitle()));
                        } else {
                            throw new RuntimeException("Employee's Job Title must be an existent Job Title");
                        }
                    } else {
                        throw new RuntimeException("Employee's Job Title is required");
                    }

                    if (employee.getId() == null) {
                        employee.setId(employeeRepository.save(employee).getId());
                    }
                });
            }
            return departmentRepository.save(department);
        }
        return null;
    }

    @Override
    public void delete(Department object) {
        departmentRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        departmentRepository.deleteById(aLong);
    }

    @Override
    public void deleteAll() {
        departmentRepository.deleteAll();
    }

    @Override
    public Department create(Department newDepartment) {
        if (newDepartment.getId() == null || !departmentRepository.existsById(newDepartment.getId())) {
            return departmentRepository.save(newDepartment);
        }
        return null;
    }

    @Override
    public Department findByIdOrDescription(Long id, String description) {
        return departmentRepository.findByIdOrDescription(id, description);
    }
}
