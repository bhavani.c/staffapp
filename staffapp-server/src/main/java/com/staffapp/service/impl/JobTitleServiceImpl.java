package com.staffapp.service.impl;

import com.staffapp.model.JobTitle;
import com.staffapp.repositories.JobTitleRepository;
import com.staffapp.service.JobTitleService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class JobTitleServiceImpl implements JobTitleService {

    private final JobTitleRepository jobTitleRepository;

    public JobTitleServiceImpl(JobTitleRepository jobTitleRepository) {
        this.jobTitleRepository = jobTitleRepository;
    }

    @Override
    public Set<JobTitle> findAll() {
        Set<JobTitle> jobTitles = new HashSet<>();
        jobTitleRepository.findAll().forEach(jobTitles::add);
        return jobTitles;
    }

    @Override
    public JobTitle findById(Long aLong) {
        return jobTitleRepository.findById(aLong).orElse(null);
    }

    @Override
    public JobTitle save(JobTitle jobTitle) {
        if(jobTitle != null) {
            return jobTitleRepository.save(jobTitle);
        }
        return null;
    }

    @Override
    public void delete(JobTitle object) {
        jobTitleRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        jobTitleRepository.deleteById(aLong);
    }

    @Override
    public void deleteAll() {
        jobTitleRepository.deleteAll();
    }

    @Override
    public JobTitle findByIdOrDescription(Long id, String description) {
        return jobTitleRepository.findByIdOrDescription(id, description);
    }
}
