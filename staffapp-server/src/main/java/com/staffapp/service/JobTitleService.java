package com.staffapp.service;

import com.staffapp.model.JobTitle;

public interface JobTitleService extends CrudService<JobTitle, Long> {
    JobTitle findByIdOrDescription(Long id, String description);
}
