package com.staffapp.config;

import com.staffapp.StaffappServerApp;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = StaffappServerApp.class)
public class StaffAppConfig {
}
