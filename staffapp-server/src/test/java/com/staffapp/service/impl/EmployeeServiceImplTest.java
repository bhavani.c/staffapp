package com.staffapp.service.impl;

import com.staffapp.StaffappServerApp;
import com.staffapp.model.Department;
import com.staffapp.model.Employee;
import com.staffapp.model.JobTitle;
import com.staffapp.repositories.EmployeeRepository;
import com.staffapp.repositories.JobTitleRepository;
import com.staffapp.service.EmployeeService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles({"integration-test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StaffappServerApp.class)
public class EmployeeServiceImplTest {

    @Mock
    EmployeeRepository employeeRepository;
    @Mock
    JobTitleRepository jobTitleRepository;

    EmployeeService service;

    Employee employee1;
    Employee employee2;
    Set<Employee> employees;
    JobTitle jobTitle;
    Department department;

    @Rule
    public MockitoRule initRule = MockitoJUnit.rule();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Before
    public void setUp() throws Exception{
        service = new EmployeeServiceImpl(employeeRepository, jobTitleRepository);

        employees = new HashSet<>();

        employee1 = new Employee();
        employee1.setFirstName("FirstName");
        employee1.setLastName("LastName");
        employee1.setId(1L);

        employee2 = new Employee();
        employee2.setFirstName("FirstName");
        employee2.setLastName("LastName");
        employee2.setId(2L);

        employees.add(employee1);
        employees.add(employee2);

        jobTitle = new JobTitle();
        jobTitle.setDescription("Job Title");
        jobTitle.setId(1L);

        department = new Department();
        department.setDescription("Department");
        department.setId(1L);
        department.setEmployees(new HashSet<>(Arrays.asList(employee1, employee2)));

    }

    @Test
    public void findAll() {
        when(employeeRepository.findAll()).thenReturn(employees);

        Set<Employee> returnedEmployees = service.findAll();
        assertThat(returnedEmployees, Matchers.notNullValue());
        assertThat(returnedEmployees, Matchers.hasSize(2));
    }

    @Test
    public void findById_NotFound() {
        when(employeeRepository.findById(anyLong())).thenReturn(null);

        exceptionRule.expect(RuntimeException.class);
        Employee employee = service.findById(1L);

        assertThat(employee, Matchers.nullValue());
    }

    @Test
    public void findById_Found() {
        when(employeeRepository.findById(anyLong())).thenReturn(Optional.of(employee1));

        Employee employee = service.findById(1L);

        assertThat(employee, Matchers.notNullValue());
        assertThat(employee.getId(), Matchers.comparesEqualTo(employee1.getId()));
    }

    @Test
    public void save_NullEmployee() {
        Employee employee = service.save(null);
        assertNull(employee);
        verify(jobTitleRepository, times(0)).save(any());
        verify(employeeRepository, times(0)).save(any());
    }

    @Test
    public void save_EmployeeNoJobTitle() {
        Employee employee = new Employee();
        employee.setFirstName("FirstName");
        employee.setLastName("LastName");
        employee.setId(2L);

        exceptionRule.expect(RuntimeException.class);
        Employee createdEmployee = service.save(employee);
        assertNull(createdEmployee);
        verify(jobTitleRepository, times(0)).save(any());
        verify(employeeRepository, times(0)).save(any());
    }

    @Test
    public void save_EmployeeInvalidJobTitle() {
        Employee employee= new Employee();
        employee.setFirstName("FirstName");
        employee.setLastName("LastName");
        employee.setId(3L);
        employee.setJobTitle(jobTitle);

        when(jobTitleRepository.existsById(anyLong())).thenReturn(false);

        exceptionRule.expect(RuntimeException.class);
        Employee createdEmployee = service.save(employee);
        assertNull(createdEmployee);
        verify(jobTitleRepository, times(0)).save(any());
        verify(employeeRepository, times(0)).save(any());
    }

    @Test
    public void save_ValidEmployee() {
        Employee employee = new Employee();
        employee.setFirstName("FirstName");
        employee.setLastName("LastName");
        employee.setId(2L);
        employee.setJobTitle(jobTitle);

        when(jobTitleRepository.existsById(anyLong())).thenReturn(true);
        when(employeeRepository.save(any())).thenReturn(employee);

        assertThat(service.save(employee), Matchers.notNullValue());
        verify(employeeRepository, times(1)).save(any());
    }

    @Test
    public void delete() {
        service.delete(employee1);
        verify(employeeRepository).delete(any());
    }

    @Test
    public void deleteById() {
        service.deleteById(1L);
        verify(employeeRepository).deleteById(anyLong());
    }

    @Test
    public void create_New() {
        when(employeeRepository.save(any())).thenReturn(employee1);

        Employee createdEmployee = service.create(employee1);
        assertNotNull(createdEmployee);
        verify(employeeRepository, times(1)).save(any());
    }

    @Test
    public void create_Existing() {
        when(employeeRepository.existsById(1L)).thenReturn(true);

        Employee createdEmployee = service.create(employee1);
        assertNull(createdEmployee);
        verify(employeeRepository, times(0)).save(any());
    }

    @Test
    public void findByNames_NoMiddleName() {
        when(employeeRepository.findByFirstNameOrLastName(anyString(), anyString())).thenReturn(employees);

        Set<Employee> foundEmployees = service.findByNames(employee1.getFirstName(),
                employee1.getLastName(), employee1.getMiddleName());

        assertNotNull(foundEmployees);

        assertEquals(employees.stream().findFirst().get().getFirstName(), "FirstName");
        assertEquals(employees.stream().findFirst().get().getLastName(), "LastName");
        assertNull(employees.stream().findFirst().get().getMiddleName());

        verify(employeeRepository).findByFirstNameOrLastName(anyString(), anyString());
    }

    @Test
    public void findByNames() {
        employee1.setMiddleName("MiddleName");

        when(employeeRepository.findByFirstNameOrLastNameAndMiddleName(anyString(), anyString(), anyString()))
                .thenReturn(employees);

        Set<Employee> foundEmployees = service.findByNames(employee1.getFirstName(),
                employee1.getLastName(), employee1.getMiddleName());

        assertNotNull(foundEmployees);

        assertEquals(employees.stream().findFirst().get().getFirstName(), "FirstName");
        assertEquals(employees.stream().findFirst().get().getLastName(), "LastName");
        assertEquals(employees.stream().findFirst().get().getMiddleName(), "MiddleName"); //TODO: check null

        verify(employeeRepository).findByFirstNameOrLastNameAndMiddleName(anyString(), anyString(), anyString());
    }

    @Test
    public void deleteAll() {
        when(employeeRepository.findAll()).thenReturn(employees);

        service.deleteAll();
        verify(employeeRepository).deleteAll();
    }

}