package com.staffapp.service.impl;

import com.staffapp.StaffappServerApp;
import com.staffapp.model.Department;
import com.staffapp.model.Employee;
import com.staffapp.model.JobTitle;
import com.staffapp.repositories.DepartmentRepository;
import com.staffapp.repositories.EmployeeRepository;
import com.staffapp.repositories.JobTitleRepository;
import com.staffapp.service.DepartmentService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles({"integration-test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StaffappServerApp.class)
public class DepartmentServiceImplTest {

    @Mock
    EmployeeRepository employeeRepository;
    @Mock
    JobTitleRepository jobTitleRepository;
    @Mock
    DepartmentRepository departmentRepository;

    DepartmentService service;

    Set<Department> departments;
    Employee employee1;
    Employee employee2;
    JobTitle jobTitle;
    Department department1;

    @Rule
    public MockitoRule initRule = MockitoJUnit.rule();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        service = new DepartmentServiceImpl(departmentRepository, employeeRepository, jobTitleRepository);

        departments = new HashSet<>();

        employee1 = new Employee();
        employee1.setFirstName("FirstName");
        employee1.setLastName("LastName");
        employee1.setId(1L);

        employee2 = new Employee();
        employee2.setFirstName("FirstName");
        employee2.setLastName("LastName");
        employee2.setId(2L);

        jobTitle = new JobTitle();
        jobTitle.setDescription("Job Title");
        jobTitle.setId(1L);

        department1 = new Department();
        department1.setDescription("Department");
        department1.setId(1L);
        department1.setEmployees(new HashSet<>(Arrays.asList(employee1, employee2)));

        departments.add(department1);
    }

    @Test
    public void findAll() {
        when(departmentRepository.findAll()).thenReturn(departments);

        Set<Department> returnedDepartments = service.findAll();
        assertThat(returnedDepartments, Matchers.notNullValue());
        assertThat(returnedDepartments, Matchers.hasSize(1));
    }

    @Test
    public void findById_NotFound() {
        when(departmentRepository.findById(anyLong())).thenReturn(null);

        exceptionRule.expect(RuntimeException.class);
        Department department = service.findById(1L);

        assertThat(department, Matchers.nullValue());
        verify(departmentRepository).findById(anyLong());
    }

    @Test
    public void findById_Found() {
        when(departmentRepository.findById(anyLong())).thenReturn(Optional.of(department1));

        Department department = service.findById(1L);

        assertThat(department, Matchers.notNullValue());
        assertThat(department.getId(), Matchers.comparesEqualTo(department1.getId()));
    }

    @Test
    public void save_NullDepartment() {
        Department department = service.save(null);

        assertNull(department);
        verify(departmentRepository, times(0)).save(any());
    }

    @Test
    public void save_NoJobTitle() {
        Employee employee = new Employee();
        Department newDepartment = new Department();
        newDepartment.setEmployees(new HashSet<>(Arrays.asList(employee, employee1)));

        exceptionRule.expect(RuntimeException.class);
        Department department = service.save(newDepartment);

        verify(jobTitleRepository, times(0)).save(any());
        verify(employeeRepository, times(0)).save(any());
        verify(departmentRepository, times(0)).save(any());
    }

    @Test
    public void save_NoEmployee() {
        Department newDepartment = new Department();
        when(departmentRepository.save(any())).thenReturn(newDepartment);

        Department department = service.save(newDepartment);

        assertNotNull(department);
        verify(jobTitleRepository, times(0)).save(any());
        verify(employeeRepository, times(0)).save(any());
        verify(departmentRepository).save(any());
    }

    @Test
    public void save_NotValidJobTitle() {
        Employee employee = new Employee();
        JobTitle jobTitle = new JobTitle();
        employee.setJobTitle(jobTitle);
        Department newDepartment = new Department();
        newDepartment.setEmployees(new HashSet<>(Arrays.asList(employee, employee1)));

        exceptionRule.expect(RuntimeException.class);
        Department department = service.save(newDepartment);

        verify(jobTitleRepository, times(0)).save(any());
        verify(employeeRepository, times(0)).save(any());
        verify(departmentRepository, times(0)).save(any());
    }

    @Test
    public void delete() {
        service.delete(department1);
        verify(departmentRepository).delete(any());
    }

    @Test
    public void deleteById() {
        service.deleteById(1L);
        verify(departmentRepository).deleteById(anyLong());
    }

    @Test
    public void deleteAll() {
        when(departmentRepository.findAll()).thenReturn(departments);

        service.deleteAll();
        verify(departmentRepository).deleteAll();
    }

    @Test
    public void create_New() {
        when(departmentRepository.save(any())).thenReturn(department1);

        Department createdDepartment = service.create(department1);
        assertNotNull(createdDepartment);
        verify(departmentRepository, times(1)).save(any());
    }

    @Test
    public void create_Existing() {
        when(departmentRepository.existsById(1L)).thenReturn(true);

        Department createdDepartment = service.create(department1);
        assertNull(createdDepartment);
        verify(departmentRepository, times(0)).save(any());
    }

    @Test
    public void findByDescription() {
        when(departmentRepository.findByIdOrDescription(any(), anyString())).thenReturn(department1);

        Department foundDepartment = service.findByIdOrDescription(null, "Department");
        assertNotNull(foundDepartment);
        assertEquals(foundDepartment.getDescription(), "Department");
        verify(departmentRepository).findByIdOrDescription(any(), anyString());

    }
}