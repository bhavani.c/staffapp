package com.staffapp.service.impl;

import com.staffapp.StaffappServerApp;
import com.staffapp.model.JobTitle;
import com.staffapp.repositories.JobTitleRepository;
import com.staffapp.service.JobTitleService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles({"integration-test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StaffappServerApp.class)
public class JobTitleServiceImplTest {

    @Mock
    JobTitleRepository repository;

    JobTitleService service;

    @Rule
    public MockitoRule initRule = MockitoJUnit.rule();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    JobTitle jobTitle1;
    JobTitle jobTitle2;
    Set<JobTitle> jobTitles;

    @Before
    public void setUp() throws Exception {
        service = new JobTitleServiceImpl(repository);

        jobTitles = new HashSet<>();

        jobTitle1 = new JobTitle();
        jobTitle1.setDescription("Job Title 1");
        jobTitle1.setId(1L);

        jobTitle2 = new JobTitle();
        jobTitle2.setDescription("Job Title 2");
        jobTitle2.setId(2L);

        jobTitles.add(jobTitle1);
        jobTitles.add(jobTitle2);
    }

    @Test
    public void findAll() {
        when(repository.findAll()).thenReturn(jobTitles);

        Set<JobTitle> returnedJobTitles = service.findAll();
        assertThat(returnedJobTitles, Matchers.notNullValue());
        assertThat(returnedJobTitles, Matchers.hasSize(2));
    }

    @Test
    public void findById_NotFound() {
        when(repository.findById(anyLong())).thenReturn(null);

        exceptionRule.expect(RuntimeException.class);
        JobTitle foundJobTitle = service.findById(1L);
        assertNull(foundJobTitle);
    }

    @Test
    public void save_NotNull() {
        when(repository.save(any())).thenReturn(jobTitle1);

        assertThat(service.save(jobTitle1), Matchers.notNullValue());
        verify(repository, times(1)).save(any());
    }

    @Test
    public void save_Null() {
        assertThat(service.save(null), Matchers.nullValue());
        verify(repository, times(0)).save(any());
    }

    @Test
    public void delete() {
        service.delete(jobTitle1);
        verify(repository).delete(any());
    }

    @Test
    public void deleteById() {
        service.deleteById(1L);
        verify(repository).deleteById(anyLong());
    }

    @Test
    public void deleteAll() {
        when(repository.findAll()).thenReturn(jobTitles);

        service.deleteAll();
        verify(repository).deleteAll();
    }

    @Test
    public void findByDescriptionOnly() {
        when(repository.findByIdOrDescription(any(), anyString())).thenReturn(jobTitle1);

        JobTitle foundJobTitle = service.findByIdOrDescription(null, "Job Title 1");
        assertNotNull(foundJobTitle);
        assertEquals(foundJobTitle.getDescription(), "Job Title 1");
        verify(repository).findByIdOrDescription(any(), anyString());
    }

    @Test
    public void findByIdOnly() {
        when(repository.findByIdOrDescription(anyLong(), any())).thenReturn(jobTitle1);

        JobTitle foundJobTitle = service.findByIdOrDescription(1L, null);
        assertNotNull(foundJobTitle);
        assertEquals(foundJobTitle.getDescription(), "Job Title 1");
        verify(repository).findByIdOrDescription(anyLong(), any());
    }
}