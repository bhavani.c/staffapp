package com.staffapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.staffapp.StaffappServerApp;
import com.staffapp.model.JobTitle;
import com.staffapp.service.JobTitleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles({"integration-test"})
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = StaffappServerApp.class)
public class JobTitleControllerTest {
    private static final String LIST_ALL = "/jobtitle/list";
    private static final String GET_ID_1 = "/jobtitle/find?id=1";
    private static final String GET_DESCRIPTION_1 = "/jobtitle/find?desc=Job%20Title1";
    private static final String SAVE = "/jobtitle/save-update";
    private static final String DELETE_ID = "/jobtitle/delete?id=1";
    private static final String DELETE_ALL = "/jobtitle/deleteAll";

    @Mock
    JobTitleService jobTitleService;

    @InjectMocks
    JobTitleController controller;

    Set<JobTitle> jobTitles;
    JobTitle jobTitle1;
    JobTitle jobTitle2;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {

        jobTitles = new HashSet<>();
        jobTitle1 = new JobTitle();
        jobTitle1.setDescription("Job Title1");
        jobTitle1.setId(1L);
        jobTitle2 = new JobTitle();
        jobTitle2.setDescription("Job Title2");
        jobTitle1.setId(2L);
        jobTitles.add(jobTitle1);
        jobTitles.add(jobTitle2);

        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();

    }

    @Test
    public void getAllJobTitles() throws Exception {
        when(jobTitleService.findAll()).thenReturn(jobTitles);

        mockMvc.perform(get(LIST_ALL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    public void findByIdOnly() throws Exception {
        when(jobTitleService.findByIdOrDescription(anyLong(), any())).thenReturn(jobTitle1);

        mockMvc.perform(get(GET_ID_1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(4)))
                .andExpect(jsonPath("$.description").value("Job Title1"));
    }

    @Test
    public void findByDescriptionOnly() throws Exception {
        when(jobTitleService.findByIdOrDescription(anyLong(), any())).thenReturn(jobTitle1);

        mockMvc.perform(get(GET_DESCRIPTION_1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(4)))
                .andExpect(jsonPath("$.description").value("Job Title1"));
    }

    @Test
    public void saveJobTitle() throws Exception {
        when(jobTitleService.save(any())).thenReturn(jobTitle1);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jobTitle1);

        mockMvc.perform(post(SAVE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("Job Title1"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void deleteById() throws Exception {
        mockMvc.perform(delete(DELETE_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void deleteAllJobTitles() throws Exception {
        mockMvc.perform(delete(DELETE_ALL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    protected HttpHeaders getHeaders() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return requestHeaders;
    }
}