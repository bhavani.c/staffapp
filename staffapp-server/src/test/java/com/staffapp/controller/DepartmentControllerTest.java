package com.staffapp.controller;

import com.staffapp.StaffappServerApp;
import com.staffapp.model.Department;
import com.staffapp.service.DepartmentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles({"integration-test"})
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = StaffappServerApp.class)
public class DepartmentControllerTest {

    @Mock
    DepartmentService departmentService;

    @InjectMocks
    DepartmentController controller;

    Set<Department> departments;
    Department department1;
    Department department2;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        departments = new HashSet<>();
    }

    @Test
    public void getAllDepartments() {
    }

    @Test
    public void findByIdOrDescription() {
    }

    @Test
    public void addNewDepartment() {
    }

    @Test
    public void updateDepartment() {
    }

    @Test
    public void deleteById() {
    }

    @Test
    public void deleteAllDepartments() {
    }
}